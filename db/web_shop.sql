drop schema if exists web_shop;
create schema web_shop default character set utf8;
use web_shop;

create table user (
	id int not null auto_increment,
    username varchar (30) not null unique,
    password varchar (30) not null unique,
    admin boolean,
    primary key (id)
);

create table category (
	id int not null auto_increment,
    name varchar (30) not null unique,
    primary key (id)
);

create table product (
	id int not null auto_increment,
    name varchar (50) not null,
    price float (10, 2) not null,
    category int not null,
    primary key (id),
    foreign key (category) references category (id)
);

insert into user (username, password, admin) values ('pera', 'peric', true);
insert into user (username, password, admin) values ('steva', 'stevic', false);
insert into user (username, password, admin) values ('mika', 'mikic', true);

insert into category (name) values ('tehnika');
insert into category (name) values ('hrana');
insert into category (name) values ('odeca');
insert into category (name) values ('obuca');

insert into product (name, price, category) values ('Televizor marke Sony', 22000, 1);
insert into product (name, price, category) values ('Sony digitalna kamera', 32000, 1);
insert into product (name, price, category) values ('Dupla plazma', 200, 2);
insert into product (name, price, category) values ('Nike patike', 7100, 4);