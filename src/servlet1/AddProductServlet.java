package servlet1;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import servlet1.dao.CategoryDAO;
import servlet1.dao.ProductDAO;
import servlet1.webshop.Category;
import servlet1.webshop.Product;
import servlet1.webshop.User;

public class AddProductServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

	response.sendRedirect("addProduct.jsp");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	HttpSession session = request.getSession();
	User user = (User) session.getAttribute("user");

	if (user == null) {
	    response.sendRedirect("index.html");
	    return;
	}

	String productName = request.getParameter("new_product_name");
	double productPrice = Double.parseDouble(request.getParameter("new_product_price"));
	int prodCategoryID = Integer.parseInt(request.getParameter("new_product_categoryID"));

	Category newProductCategory = CategoryDAO.getByID(prodCategoryID);
	Product product = new Product(0, productName, productPrice, newProductCategory);
	ProductDAO.add(product);

	response.sendRedirect("webShop.jsp");
    }

}
