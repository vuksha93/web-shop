package servlet1.webshop;

public class Category {

    private int id;
    private String name;

    private Products products;

    public Category() {
    }

    public Category(int id, String name) {
	this.id = id;
	this.name = name;
    }

    @Override
    public String toString() {
	return String.format("%s", name);
    }

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public Products getProducts() {
	return products;
    }

    public void setProducts(Products products) {
	this.products = products;
    }

}
