package servlet1.webshop;

public class User {

    private int id;
    private String username;
    private String password;
    private boolean isAdministrator;

    private ShoppingCart cart = new ShoppingCart();

    public User() {
    }

    public User(int id, String username, String password, boolean isAdministrator) {
	this.id = id;
	this.username = username;
	this.password = password;
	this.isAdministrator = isAdministrator;
    }

    @Override
    public String toString() {
	return String.format("%s %s", username, password);
    }

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getUsername() {
	return username;
    }

    public void setUsername(String username) {
	this.username = username;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public ShoppingCart getCart() {
	return cart;
    }

    public void setCart(ShoppingCart cart) {
	this.cart = cart;
    }

    public boolean isAdministrator() {
	return isAdministrator;
    }

    public void setAdministrator(boolean isAdministrator) {
	this.isAdministrator = isAdministrator;
    }
    
    public int getAdminInt() {
	if(this.isAdministrator) {
	    return 1;
	}
	
	return 0;
    }

}
