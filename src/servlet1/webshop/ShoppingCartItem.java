package servlet1.webshop;

public class ShoppingCartItem {

    private int id;
    private Product product;
    private int count;

    public ShoppingCartItem(int id, Product p, int count) {
	this.id = id;
	this.product = p;
	this.count = count;
    }

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public void setProduct(Product p) {
	product = p;
    }

    public Product getProduct() {
	return product;
    }

    public void setCount(int c) {
	count = c;
    }

    public int getCount() {
	return count;
    }

}
