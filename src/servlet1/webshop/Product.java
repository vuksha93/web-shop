package servlet1.webshop;

public class Product {

    private int id;
    private String name;
    private double price;
    private Category category;

    public Product(int id, String name, double price) {
	this.id = id;
	this.name = name;
	this.price = price;
    }

    public Product(int id, String name, double price, Category category) {
	this.id = id;
	this.name = name;
	this.price = price;
	this.category = category;
    }

    @Override
    public String toString() {
	return String.format("%s %s %s %s", id, name, price, category.getName());
    }

    public String toFileRepresentation() {
	return String.format("%s;%s;%s", id, name, price);
    }

    public void setId(int i) {
	id = i;
    }

    public int getId() {
	return id;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public double getPrice() {
	return price;
    }

    public void setPrice(double price) {
	this.price = price;
    }

    public Category getCategory() {
	return category;
    }

    public void setCategory(Category category) {
	this.category = category;
    }

}
