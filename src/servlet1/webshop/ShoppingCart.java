package servlet1.webshop;

import java.util.ArrayList;

public class ShoppingCart {

    private ArrayList<ShoppingCartItem> items;

    public ShoppingCart() {
	items = new ArrayList<ShoppingCartItem>();
    }

    public void addItem(Product product, int count) {
	int id = items.isEmpty() ? 1 : (items.get(items.size() - 1).getId() + 1);

	items.add(new ShoppingCartItem(id, product, count));
    }

    public void addNewItem(ShoppingCartItem shoppingItem) {
	items.add(shoppingItem);
    }
    
    public void addNewItems(ArrayList<ShoppingCartItem> shoppingItems) {
	items.addAll(shoppingItems);
    }

    public void deleteItem(int id) {
	for (ShoppingCartItem item : items) {
	    if (item.getId() == id) {
		items.remove(item);
		break;
	    }
	}
    }

    public ArrayList<ShoppingCartItem> getItems() {
	return items;
    }
    
    public int getNextId() {
	int maxId = 0;
	for(ShoppingCartItem item : items) {
	    if(item.getId() > maxId) {
		maxId = item.getId();
	    }
	}
	return ++maxId;
    }
}
