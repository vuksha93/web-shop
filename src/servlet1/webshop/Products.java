package servlet1.webshop;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.StringTokenizer;

import servlet1.dao.ProductDAO;

public class Products {

    private HashMap<Integer, Product> products = new HashMap<Integer, Product>();
  
    public Products(String path) {
	BufferedReader in = null;
	try {
	    File file = new File(path + "/products.txt");
	    System.out.println(file.getCanonicalPath());
	    in = new BufferedReader(new FileReader(file));
	    readProducts(in);
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    if (in != null) {
		try {
		    in.close();
		} catch (Exception e) {
		}
	    }
	}
    }
    
    public Products(ProductDAO productDAO) {	
	products = ProductDAO.getAll();
    }

    private void readProducts(BufferedReader in) {
	String line, id = "", name = "", price = "";
	StringTokenizer st;
	try {
	    while ((line = in.readLine()) != null) {
		line = line.trim();
		if (line.equals("") || line.indexOf('#') == 0)
		    continue;
		st = new StringTokenizer(line, ";");
		while (st.hasMoreTokens()) {
		    id = st.nextToken().trim();
		    name = st.nextToken().trim();
		    price = st.nextToken().trim();
		}
		products.put(Integer.parseInt(id), new Product(Integer.parseInt(id), name, Double.parseDouble(price)));
	    }
	} catch (Exception ex) {
	    ex.printStackTrace();
	}
    }

    public void addProductsToFile(File file) throws IOException {
	FileWriter fw = new FileWriter(file);

	try {
	    for (Product pr : values()) {
		fw.write(pr.toFileRepresentation() + "\n");
	    }

	} catch (IOException e) {
	    e.printStackTrace();
	} finally {
	    fw.close();
	}
    }

    public Collection<Product> values() {
	return products.values();
    }

    public Product getProduct(int id) {
	return products.get(id);
    }

    public void addProduct(Product product) {
	products.put(product.getId(), product);
    }

    public void removeProduct(int id) {
	products.remove(id);
    }
}
