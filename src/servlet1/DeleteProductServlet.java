package servlet1;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import servlet1.dao.ProductDAO;
import servlet1.webshop.User;

public class DeleteProductServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

	HttpSession session = request.getSession();
	User user = (User) session.getAttribute("user");

	if (user == null) {
	    response.sendRedirect("index.html");
	    return;
	}

	ProductDAO.delete(Integer.parseInt(request.getParameter("productId")));
	response.sendRedirect("webShop.jsp");
    }

}
