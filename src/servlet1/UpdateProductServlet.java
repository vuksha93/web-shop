package servlet1;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servlet1.dao.CategoryDAO;
import servlet1.dao.ProductDAO;
import servlet1.webshop.Category;
import servlet1.webshop.Product;

public class UpdateProductServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
	int prodID = Integer.parseInt(request.getParameter("productId"));
	response.sendRedirect("updateProduct.jsp?productId=" + prodID);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	int prodID = Integer.parseInt(req.getParameter("prodID"));
	String prodName = req.getParameter("new_product_name");
	double prodPrice = Double.parseDouble(req.getParameter("new_product_price"));
	int prodCategoryID = Integer.parseInt(req.getParameter("new_product_categoryID"));

	Category newProductCategory = null;
	newProductCategory = CategoryDAO.getByID(prodCategoryID);

	Product updatedProd = ProductDAO.getProductByID(prodID);
	updatedProd.setName(prodName);
	updatedProd.setPrice(prodPrice);
	updatedProd.setCategory(newProductCategory);

	ProductDAO.update(updatedProd);

	resp.sendRedirect("webShop.jsp");
    }

}
