package servlet1;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import servlet1.dao.ProductDAO;
import servlet1.webshop.Product;
import servlet1.webshop.Products;
import servlet1.webshop.ShoppingCart;
import servlet1.webshop.ShoppingCartItem;
import servlet1.webshop.User;

public class ShoppingCartServlet extends HttpServlet {

    private static final long serialVersionUID = -8628509500586512294L;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	HttpSession session = request.getSession();
	User user = (User) session.getAttribute(LogInServlet.USER_KEY);

	if (user == null) {
	    response.sendRedirect("index.html");
	    return;
	}

	Products products = new Products(new ProductDAO());

	ShoppingCart sc = user.getCart();

	Map<String, String[]> params = request.getParameterMap();
	String[] itemCounts = params.get("itemCount");
	String[] itemIds = params.get("itemId");

	if (itemCounts != null) {
	    for (int i = 0; i < itemCounts.length; i++) {
		if (itemCounts[i].equals("")) {
		    continue;
		}
		int id = sc.getNextId();
		try {
		    Product product = products.getProduct(Integer.parseInt(itemIds[i]));
		    int itemCount = Integer.parseInt(itemCounts[i]);
		    ShoppingCartItem scItem = new ShoppingCartItem(id, product, itemCount);
		    sc.addNewItem(scItem);
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	}

	if (request.getParameter("deleteItemId") != null) {
	    try {
		int itemId = Integer.parseInt(request.getParameter("deleteItemId"));
		sc.deleteItem(itemId);
	    } catch (Exception ex) {
		ex.printStackTrace();
	    }
	}
	response.sendRedirect("shoppingCart.jsp");

    }

}
