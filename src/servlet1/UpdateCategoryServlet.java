package servlet1;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servlet1.dao.CategoryDAO;
import servlet1.webshop.Category;

public class UpdateCategoryServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {

	int categoryID = Integer.parseInt(request.getParameter("categoryId"));
	response.sendRedirect("updateCategory.jsp?categoryId=" + categoryID);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {

	int categoryID = Integer.parseInt(request.getParameter("categID"));
	String categName = request.getParameter("new_category_name");

	Category updatedCategory = CategoryDAO.getByID(categoryID);
	updatedCategory.setName(categName);

	CategoryDAO.update(updatedCategory);

	response.sendRedirect("category.jsp");
    }

}
