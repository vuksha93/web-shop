package servlet1.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import servlet1.webshop.Category;
import servlet1.webshop.Product;
import utils.ConnectionManager;
import utils.UtilClass;

public class ProductDAO {

    private static Connection conn = ConnectionManager.getConnection();

    public static HashMap<Integer, Product> getAll() {
	HashMap<Integer, Product> products = new HashMap<Integer, Product>();

	Statement stmt = null;
	ResultSet rset = null;

	try {
	    String query = "SELECT id, name, price, category FROM product";

	    stmt = conn.createStatement();
	    rset = stmt.executeQuery(query);

	    while (rset.next()) {
		int ind = 1;
		int id = rset.getInt(ind++);
		String name = rset.getString(ind++);
		double price = rset.getDouble(ind++);
		int categoryID = rset.getInt(ind++);

		Category category = CategoryDAO.getByID(categoryID);

		products.put(id, new Product(id, name, price, category));
	    }
	} catch (SQLException e) {
	    System.out.println("Error with query!");
	    e.printStackTrace();
	} finally {
	    UtilClass.closeStatement(stmt, rset);
	}

	return products;
    }

    public static Product getProductByID(int id) {
	Product product = null;

	PreparedStatement pstmt = null;
	ResultSet rset = null;

	try {
	    String query = "SELECT name, price, category FROM product WHERE id = ?";

	    pstmt = conn.prepareStatement(query);
	    pstmt.setInt(1, id);

	    rset = pstmt.executeQuery();
	    if (rset.next()) {
		int ind = 1;
		String name = rset.getString(ind++);
		double price = rset.getDouble(ind++);
		int categoryID = rset.getInt(ind++);

		Category category = CategoryDAO.getByID(categoryID);

		product = new Product(id, name, price, category);
	    }
	} catch (SQLException e) {
	    System.out.println("Error with query!");
	    e.printStackTrace();
	} finally {
	    UtilClass.closeStatement(pstmt, rset);
	}

	return product;
    }

    public static HashMap<Integer, Product> getProductsByCategory(int categoryID) {
	HashMap<Integer, Product> products = new HashMap<Integer, Product>();

	PreparedStatement pstmt = null;
	ResultSet rset = null;

	try {
	    String query = "SELECT id, name, price FROM product WHERE category = ?";

	    pstmt = conn.prepareStatement(query);
	    pstmt.setInt(1, categoryID);

	    rset = pstmt.executeQuery();

	    while (rset.next()) {
		int ind = 1;
		int id = rset.getInt(ind++);
		String name = rset.getString(ind++);
		double price = rset.getDouble(ind++);

		Category category = CategoryDAO.getByID(categoryID);

		products.put(id, new Product(id, name, price, category));
	    }

	} catch (SQLException e) {
	    System.out.println("Error with query!");
	    e.printStackTrace();
	} finally {
	    UtilClass.closeStatement(pstmt, rset);
	}

	return products;
    }

    public static boolean add(Product product) {
	PreparedStatement pstmt = null;

	try {
	    String query = "INSERT INTO product (name, price, category) VALUES (?, ?, ?)";

	    pstmt = conn.prepareStatement(query);
	    int ind = 1;
	    pstmt.setString(ind++, product.getName());
	    pstmt.setDouble(ind++, product.getPrice());
	    pstmt.setInt(ind++, product.getCategory().getId());

	    return pstmt.executeUpdate() == 1;
	} catch (SQLException e) {
	    System.out.println("Error with query!");
	    e.printStackTrace();
	} finally {
	    UtilClass.closeStatement(pstmt);
	}

	return false;
    }

    public static boolean update(Product product) {
	PreparedStatement pstmt = null;

	try {
	    String query = "UPDATE product SET name = ?, price = ?, category = ? WHERE id = ?";

	    pstmt = conn.prepareStatement(query);
	    int ind = 1;
	    pstmt.setString(ind++, product.getName());
	    pstmt.setDouble(ind++, product.getPrice());
	    pstmt.setInt(ind++, product.getCategory().getId());
	    pstmt.setInt(ind++, product.getId());

	    return pstmt.executeUpdate() == 1;
	} catch (SQLException e) {
	    System.out.println("Error with query!");
	    e.printStackTrace();
	} finally {
	    UtilClass.closeStatement(pstmt);
	}

	return false;
    }

    public static boolean delete(int productID) {
	PreparedStatement pstmt = null;

	try {
	    String query = "DELETE FROM product WHERE id = ?";

	    pstmt = conn.prepareStatement(query);
	    pstmt.setInt(1, productID);

	    return pstmt.executeUpdate() == 1;
	} catch (SQLException e) {
	    System.out.println("Error with query!");
	    e.printStackTrace();
	} finally {
	    UtilClass.closeStatement(pstmt);
	}

	return false;
    }

    public static boolean deleteByCategory(int categoryID) {
	PreparedStatement pstmt = null;

	try {
	    String query = "DELETE FROM product WHERE category = ?";

	    pstmt = conn.prepareStatement(query);
	    pstmt.setInt(1, categoryID);

	    pstmt.executeUpdate();
	    return true;
	} catch (SQLException e) {
	    System.out.println("Error with query!");
	    e.printStackTrace();
	} finally {
	    UtilClass.closeStatement(pstmt);
	}

	return false;
    }

    public static HashMap<Integer, Product> search(String str) {
	HashMap<Integer, Product> products = new HashMap<Integer, Product>();

	PreparedStatement pstmt = null;
	ResultSet rset = null;

	try {
	    String query = "SELECT id, name, price, category FROM product WHERE name LIKE ?";

	    pstmt = conn.prepareStatement(query);
	    pstmt.setString(1, "%" + str + "%");
	    rset = pstmt.executeQuery();

	    while (rset.next()) {
		int ind = 1;
		int id = rset.getInt(ind++);
		String name = rset.getString(ind++);
		double price = rset.getDouble(ind++);
		int categoryID = rset.getInt(ind++);
		Category category = CategoryDAO.getByID(categoryID);

		products.put(id, new Product(id, name, price, category));
	    }

	} catch (SQLException e) {
	    System.out.println("Error with query!");
	    e.printStackTrace();
	} finally {
	    UtilClass.closeStatement(pstmt, rset);
	}

	return products;
    }

}
