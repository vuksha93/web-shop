package servlet1.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import servlet1.webshop.Category;
import utils.ConnectionManager;
import utils.UtilClass;

public class CategoryDAO {

    private static Connection conn = ConnectionManager.getConnection();

    public static HashMap<Integer, Category> getAll() {
	HashMap<Integer, Category> categories = new HashMap<Integer, Category>();

	Statement stmt = null;
	ResultSet rset = null;

	try {
	    String query = "SELECT id, name FROM category ORDER BY id";

	    stmt = conn.createStatement();
	    rset = stmt.executeQuery(query);

	    while (rset.next()) {
		int ind = 1;
		int id = rset.getInt(ind++);
		String name = rset.getString(ind++);

		categories.put(id, new Category(id, name));
	    }

	} catch (SQLException e) {
	    System.out.println("Error with query!");
	    e.printStackTrace();
	} finally {
	    UtilClass.closeStatement(stmt, rset);
	}

	return categories;
    }

    public static Category getByID(int id) {
	Category category = null;

	PreparedStatement pstmt = null;
	ResultSet rset = null;

	try {
	    String query = "SELECT name FROM category WHERE id = ?";

	    pstmt = conn.prepareStatement(query);
	    pstmt.setInt(1, id);

	    rset = pstmt.executeQuery();

	    if (rset.next()) {
		String name = rset.getString(1);
		category = new Category(id, name);
	    }

	} catch (SQLException e) {
	    System.out.println("Error with query!");
	    e.printStackTrace();
	} finally {
	    UtilClass.closeStatement(pstmt, rset);
	}

	return category;
    }

    public static boolean add(Category category) {
	PreparedStatement pstmt = null;

	try {
	    String query = "INSERT INTO category (name) VALUE (?)";

	    pstmt = conn.prepareStatement(query);
	    pstmt.setString(1, category.getName());

	    return pstmt.executeUpdate() == 1;
	} catch (SQLException e) {
	    System.out.println("Error with query!");
	    e.printStackTrace();
	} finally {
	    UtilClass.closeStatement(pstmt);
	}

	return false;
    }

    public static boolean update(Category category) {
	PreparedStatement pstmt = null;

	try {
	    String query = "UPDATE category SET name = ? WHERE id = ?";

	    pstmt = conn.prepareStatement(query);
	    int ind = 1;
	    pstmt.setString(ind++, category.getName());
	    pstmt.setInt(ind++, category.getId());

	    return pstmt.executeUpdate() == 1;
	} catch (SQLException e) {
	    System.out.println("Error with query!");
	    e.printStackTrace();
	} finally {
	    UtilClass.closeStatement(pstmt);
	}

	return false;
    }

    public static boolean delete(int categoryID) {
	PreparedStatement pstmt = null;

	try {
	    conn.setAutoCommit(false);

	    boolean success = ProductDAO.deleteByCategory(categoryID);
	    if (!success) {
		throw new Exception("Delete category products error!");
	    }

	    String query = "DELETE FROM category WHERE id = ?";
	    pstmt = conn.prepareStatement(query);
	    pstmt.setInt(1, categoryID);

	    success = pstmt.executeUpdate() == 1;
	    if (!success) {
		throw new Exception("Delete category error!");
	    }

	    conn.commit();
	    return success;
	} catch (Exception e) {
	    e.printStackTrace();
	    try {
		conn.rollback();
	    } catch (SQLException e2) {
		e2.printStackTrace();
	    }
	} finally {
	    try {
		conn.setAutoCommit(true);
	    } catch (SQLException e3) {
		e3.printStackTrace();
	    }
	    try {
		pstmt.close();
	    } catch (SQLException e4) {
		e4.printStackTrace();
	    }
	}

	return false;
    }
}