package servlet1.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import servlet1.webshop.User;
import utils.ConnectionManager;
import utils.UtilClass;

public class UserDAO {

    private static Connection conn = ConnectionManager.getConnection();

    public static List<User> getAll() {
	List<User> users = new ArrayList<User>();

	Statement stmt = null;
	ResultSet rset = null;

	try {
	    String query = "SELECT id, username, password, admin FROM user";

	    stmt = conn.createStatement();
	    rset = stmt.executeQuery(query);

	    while (rset.next()) {
		int ind = 1;
		int id = rset.getInt(ind++);
		String username = rset.getString(ind++);
		String password = rset.getString(ind++);
		int admin = rset.getInt(ind++);
		boolean isAdministrator = false;
		if (admin == 1) {
		    isAdministrator = true;
		}

		users.add(new User(id, username, password, isAdministrator));
	    }

	} catch (Exception e) {
	    System.out.println("Error with query!");
	    e.printStackTrace();
	} finally {
	    UtilClass.closeStatement(stmt, rset);
	}

	return users;
    }

    public static User getUserByID(int id) {
	User user = null;

	PreparedStatement pstmt = null;
	ResultSet rset = null;

	try {
	    String query = "SELECT username, password, admin FROM user WHERE id = ?";

	    pstmt = conn.prepareStatement(query);
	    pstmt.setInt(1, id);

	    rset = pstmt.executeQuery();
	    if (rset.next()) {
		int ind = 1;
		String username = rset.getString(ind++);
		String password = rset.getString(ind++);
		int admin = rset.getInt(ind++);
		boolean isAdministrator = false;
		if (admin == 1) {
		    isAdministrator = true;
		}

		user = new User(id, username, password, isAdministrator);
	    }

	} catch (SQLException e) {
	    System.out.println("Error with query!");
	    e.printStackTrace();
	} finally {
	    UtilClass.closeStatement(pstmt, rset);
	}

	return user;
    }

    public static User getUserByUsernameAndPassword(String username, String password) {
	User user = null;

	PreparedStatement pstmt = null;
	ResultSet rset = null;

	try {
	    String query = "SELECT id, admin FROM user WHERE username = ? AND password = ?";

	    pstmt = conn.prepareStatement(query);
	    pstmt.setString(1, username);
	    pstmt.setString(2, password);

	    rset = pstmt.executeQuery();
	    if (rset.next()) {
		int ind = 1;
		int id = rset.getInt(ind++);
		int admin = rset.getInt(ind++);
		boolean isAdministrator = false;
		if (admin == 1) {
		    isAdministrator = true;
		}

		user = new User(id, username, password, isAdministrator);
	    }

	} catch (SQLException e) {
	    System.out.println("Error with query!");
	    e.printStackTrace();
	} finally {
	    UtilClass.closeStatement(pstmt, rset);
	}

	return user;
    }

    public static boolean add(User user) {
	PreparedStatement pstmt = null;

	try {
	    String query = "INSERT INTO user (username, password, admin) VALUES (?, ?, ?)";

	    pstmt = conn.prepareStatement(query);
	    int ind = 1;
	    pstmt.setString(ind++, user.getUsername());
	    pstmt.setString(ind++, user.getPassword());
	    pstmt.setInt(ind++, user.getAdminInt());

	    return pstmt.executeUpdate() == 1;
	} catch (SQLException e) {
	    System.out.println("Error with query!");
	    e.printStackTrace();
	} finally {
	    UtilClass.closeStatement(pstmt);
	}

	return false;
    }

    public static List<User> search(String str) {
	List<User> users = new ArrayList<User>();
	Connection conn = ConnectionManager.getConnection();

	PreparedStatement pstmt = null;
	ResultSet rset = null;

	try {
	    String query = "SELECT id, username, password, admin FROM user WHERE username LIKE ?";

	    pstmt = conn.prepareStatement(query);
	    pstmt.setString(1, "%" + str + "%");
	    rset = pstmt.executeQuery();

	    while (rset.next()) {
		int ind = 1;
		int id = rset.getInt(ind++);
		String username = rset.getString(ind++);
		String password = rset.getString(ind++);
		int admin = rset.getInt(ind++);
		boolean isAdministrator = false;
		if (admin == 1) {
		    isAdministrator = true;
		}

		users.add(new User(id, username, password, isAdministrator));
	    }

	} catch (SQLException e) {
	    System.out.println("Error with query!");
	    e.printStackTrace();
	} finally {
	    UtilClass.closeStatement(pstmt, rset);
	}

	return users;
    }

}
