package servlet1;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import servlet1.dao.CategoryDAO;
import servlet1.webshop.User;

public class DeleteCategoryServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {

	HttpSession session = request.getSession();
	User user = (User) session.getAttribute("user");

	if (user == null) {
	    response.sendRedirect("index.html");
	    return;
	}

	CategoryDAO.delete(Integer.parseInt(request.getParameter("categoryId")));
	response.sendRedirect("category.jsp");
    }

}
