package servlet1;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servlet1.dao.UserDAO;
import servlet1.webshop.User;
import utils.ConnectionManager;

@WebServlet("/LogInServlet")
public class LogInServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public static final String USER_KEY = "user";

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	String username = request.getParameter("username");
	String password = request.getParameter("password");

	User user = UserDAO.getUserByUsernameAndPassword(username, password);

	if (user == null) {
	    response.sendRedirect("logInError.html");
	    return;
	}

	request.getSession().setAttribute(USER_KEY, user);
	response.sendRedirect("webShop.jsp");
    }

    @Override
    public void destroy() {
	ConnectionManager.closeConnection();
    }
}
