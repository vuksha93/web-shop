package servlet1;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servlet1.dao.UserDAO;
import servlet1.webshop.User;
import utils.ConnectionManager;

@WebServlet("/UserServlet")
public class UserServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	String username = request.getParameter("username");
	String password = request.getParameter("password");

	User user = new User(0, username, password, false);

	if (!UserDAO.add(user)) {
	    System.out.println("Username already exists! Try again.");
	    response.sendRedirect("registration.html");
	    return;
	}

	response.sendRedirect("index.html");
    }

    @Override
    public void destroy() {
	ConnectionManager.closeConnection();
    }

}
