package utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UtilClass {

    public static void closeStatement(Statement stmt) {
	closeStatement(stmt, null);
    }

    public static void closeStatement(Statement stmt, ResultSet rset) {
	if (rset != null) {
	    try {
		rset.close();
	    } catch (SQLException e) {
		e.printStackTrace();
	    }
	}
	if (stmt != null) {
	    try {
		stmt.close();
	    } catch (SQLException e) {
		e.printStackTrace();
	    }
	}
    }

}
