<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ page import="servlet1.dao.UserDAO" %>
<%@ page import="servlet1.webshop.User" %>
<%@ page import="java.util.List" %>

<!DOCTYPE html>
<html>

<head>

	<script type="text/javascript">
		function greenHeader() {
			var header = document.getElementById("header");
			header.style.backgroundColor = "green";
		}
	</script>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>List of users</title>

</head>

<body>

	<% List<User> users = UserDAO.getAll(); %>
	<table border="1">
		<tr>
			<th id="header">Username</th i>
		</tr>

		<% for (User user : users) { %>
		<tr>
			<td> <%= user.getUsername() %> </td>
		</tr>
		<% } %>

	</table>

	<button onclick="greenHeader()">Green header!</button>

	<p>
		<a href="webShop.jsp">Back</a>
	</p>

</body>

</html>