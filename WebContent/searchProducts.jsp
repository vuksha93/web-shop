<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ page import="servlet1.dao.ProductDAO" %>
<%@ page import="servlet1.webshop.Product" %>
<%@ page import="java.util.HashMap" %> 
    
<%@ include file="checkUser.jsp" %>
<%-- <jsp:include page="checkUser.jsp" /> --%>    
        
<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Search products</title>

</head>
<body>

	<form action="searchProducts.jsp" method="get">
        Enter product: <input type="text" name="product" autofocus /><br/>            
        <input type="submit" value="Search"> 
    </form>
	
	<% 
	String searchParam = request.getParameter("product");
	if(searchParam != null) {
	    String strProduct = searchParam.toLowerCase(); 
		HashMap<Integer, Product> products = ProductDAO.search(strProduct);
	
		for (Product product : products.values()) {  %>
		    <p> <%= product %> </p>
		<% } %>
		
	<% } %>
			
	<p>
		<a href="webShop.jsp">Back</a>
    </p>

</body>
</html>