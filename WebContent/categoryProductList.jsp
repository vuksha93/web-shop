<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ page import="servlet1.dao.ProductDAO" %>
<%@ page import="servlet1.webshop.Product" %>
<%@ page import="java.util.HashMap" %>   
    
<%@ include file="checkUser.jsp" %>
<%-- <jsp:include page="checkUser.jsp" /> --%>
    
<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>List of category products</title>

</head>
<body>

	<%  
    int categoryID = Integer.parseInt(request.getParameter("categoryId")); 
	HashMap<Integer, Product> products = ProductDAO.getProductsByCategory(categoryID); 

   	for (Product product : products.values()) { %>
		<p> <%= product %> </p>		
	<% } %>
	
	<p>
	<a href="category.jsp">Back</a>
	</p>

</body>
</html>