<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ page import="servlet1.webshop.Category" %>
<%@ page import="servlet1.webshop.Product" %>
<%@ page import="servlet1.webshop.Products" %>
<%@ page import="servlet1.dao.CategoryDAO" %>
<%@ page import="servlet1.dao.ProductDAO" %>
<%@ page import="java.util.HashMap" %>

<%@ include file="checkUser.jsp" %>
<%-- <jsp:include page="checkUser.jsp" /> --%>
    
<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Update product</title>

</head>
<body>
	<% Products products = new Products(new ProductDAO());
	HashMap<Integer, Category> categories = CategoryDAO.getAll(); %>
		
	<% 
	int prodID = Integer.parseInt(request.getParameter("productId"));
	Product updatedProd = products.getProduct(prodID);
	%> 
	    
	<h3>Update product</h3>
    <form action="UpdateProductServlet" method="post">
	    Enter new product name: 
	    <input type="text" name="new_product_name" 
	    	value="<%=updatedProd.getName()%>" autofocus 
		/>
		<br />
		Enter new product price: 
		<input type="number" name="new_product_price" 
			value="<%=updatedProd.getPrice()%>"
		/>
		<br />
		Select new category:
		<select name="new_product_categoryID">
			<% for(Category category : categories.values()) { %>
				<option value="<%= category.getId()%>"><%= category.getName() %></option>
			<% } %>
		</select><br />	    
	    <input type="hidden" name="prodID" value="<%= updatedProd.getId() %>">
	    <input type="submit" value="Update">
    </form>	

</body>
</html>