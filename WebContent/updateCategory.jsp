<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ page import="servlet1.dao.CategoryDAO" %>
<%@ page import="servlet1.webshop.Category" %>
<%@ page import="utils.ConnectionManager" %>

<%@ include file="checkUser.jsp" %>
<%-- <jsp:include page="checkUser.jsp" /> --%>
    
<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Update category</title>

</head>
<body>

	<% 
    int categoryID = Integer.parseInt(request.getParameter("categoryId"));
    Category updatedCategory = CategoryDAO.getByID(categoryID); 
    %>	    

    <h3>Update category</h3>
    <form action="UpdateCategoryServlet" method="post">
    Enter new category name: <input type="text" name="new_category_name" 
    value="<%= updatedCategory.getName() %>" autofocus /><br />
	<input type="hidden" name="categID" value="<%= updatedCategory.getId() %>"/>
    <input type="submit" value="Update"/>
    </form>	

</body>
</html>