<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ page import="servlet1.dao.CategoryDAO" %>
<%@ page import="servlet1.webshop.Category" %>
<%@ page import="java.util.HashMap" %>

<%@ include file="checkUser.jsp" %>
<%-- <jsp:include page="checkUser.jsp" /> --%>

<!DOCTYPE html>
<html>

<head>

	<script type="text/javascript">
		function greenHeader() {
			var header = document.getElementById("header");
			header.style.backgroundColor = "green";
		}
	</script>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Category</title>

</head>

<body>
	<% HashMap<Integer, Category> categories = CategoryDAO.getAll(); %>
	<h3>Available categories:</h3>

	<table border="1">
		<tr id="header" bgcolor="lightgrey">
			<th>Name</th>
			<th>&nbsp;</th>
		</tr>

		<% for (Category category : categories.values()) { %>
		<tr>
			<td> <%= category.getName() %> </td>
			<td>
				<a href="categoryProductList.jsp?categoryId=<%= category.getId() %>">View all</a>

				<% if (user != null && user.isAdministrator()) { %>
				<a href="UpdateCategoryServlet?categoryId=<%= category.getId() %>">Update</a>
				<a href="DeleteCategoryServlet?categoryId=<%= category.getId() %>">Delete</a>
				<% } %>

			</td>
		</tr>
		<% } %>

	</table>

	<button onclick="greenHeader()">Green header!</button>

	<% if (user.isAdministrator()) { %>
	<p>
		<a href="addCategory.jsp">Add new category</a>
	</p>
	<% } %>

	<p>
		<a href="webShop.jsp">Back</a>
	</p>

</body>

</html>