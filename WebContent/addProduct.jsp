<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="servlet1.webshop.Products" %>
<%@ page import="servlet1.webshop.Product" %>
<%@ page import="servlet1.webshop.Category" %>
<%@ page import="servlet1.dao.CategoryDAO" %>
<%@ page import="java.util.HashMap" %>

<%@ include file="checkUser.jsp" %>
<%-- <jsp:include page="checkUser.jsp" /> --%>

<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add product</title>

</head>
<body>
	<% HashMap<Integer, Category> categories = CategoryDAO.getAll(); %>	
	
   	<h3>Add new product</h3>
    <form action="AddProductServlet" method="post">
    	Enter product name: <input type="text" name="new_product_name" autofocus /><br />
		Enter product price: <input type="text" name="new_product_price" /><br />
		Select category: 
		<select name="new_product_categoryID">
			<% for(Category category : categories.values()) { %>
				<option value="<%= category.getId()%>"><%= category.getName() %></option>
			<% } %>
		</select>
	    <input type="submit" value="Add" />
    </form>
	
</body>
</html>