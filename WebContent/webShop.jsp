<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="servlet1.webshop.Products" %>
<%@ page import="servlet1.webshop.Product" %>
<%@ page import="servlet1.dao.ProductDAO" %>

<%@ include file="checkUser.jsp" %>
<%-- <jsp:include page="checkUser.jsp" /> --%>

<!DOCTYPE html>
<html>

<head>

	<script type="text/javascript">
		function greenHeader() {
			var header = document.getElementById("header");
			header.style.backgroundColor = "green";
		}
	</script>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Web Shop</title>

</head>

<body>
	<% Products products = new Products(new ProductDAO()); %>
	<h3>Available products:</h3>

	<form method="post" action="ShoppingCartServlet">
		<table border="1">
			<tr id="header" bgcolor="lightgrey">
				<th>Name</th>
				<th>Price</th>
				<th>Category</th>
				<th>&nbsp;</th>
			</tr>
			<% for (Product p : products.values()) { %>
			<tr>
				<td><%= p.getName()  %></td>
				<td><%= p.getPrice() %></td>
				<td><%= p.getCategory().getName() %></td>
				<td>
					<input type="text" size="3" name="itemCount" />
					<input type="hidden" name="itemId" value="<%=p.getId()%>" />

					<% if (user != null && user.isAdministrator()) { %>
					<a href="UpdateProductServlet?productId=<%= p.getId() %>">Update</a>
					<a href="DeleteProductServlet?productId=<%= p.getId() %>">Delete</a>
					<% } %>

				</td>
			</tr>
			<% } %>
		</table>
		<input type="submit" value="Dodaj">
	</form>

	<button onclick="greenHeader()">Green header!</button>

	<% if (user.isAdministrator()) { %>
	<p>
		<a href="AddProductServlet">Add new product</a>
	</p>
	<% } %>

	<p>
		<a href="shoppingCart.jsp">Shopping Cart</a>
	</p>

	<p>
		<a href="category.jsp">Categories</a>
	</p>

	<p>
		<a href="usersList.jsp">List of users</a>
	</p>

	<p>
		<a href="searchProducts.jsp">Search products</a>
	</p>

	<p>
		<a href="searchUsers.jsp">Search users</a>
	</p>

	<p>
		<a href="LogOutServlet">Log out</a>
	</p>

</body>

</html>