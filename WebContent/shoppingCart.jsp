<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ page import="servlet1.dao.ProductDAO" %>
<%@ page import="servlet1.webshop.Product" %>
<%@ page import="servlet1.webshop.Products" %>
<%@ page import="servlet1.webshop.ShoppingCart" %>
<%@ page import="servlet1.webshop.ShoppingCartItem" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Map" %>

<%@ include file="checkUser.jsp" %>
<%-- <jsp:include page="checkUser.jsp" /> --%>

<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Shopping cart</title>

</head>
<body>
	<% ShoppingCart sc = user.getCart(); %>	

    <p>Items in cart:</p>
    <table>
    	<tr bgcolor="lightgrey">
    		<th>Name</th>
    		<th>Price</th>
	    	<th>Number of items</th>
	    	<th>Total price</th>
	    	<th>&nbsp;</th>
	    </tr>
    <% double total = 0;
    for (ShoppingCartItem i : sc.getItems()) { %>
		<% double price = i.getProduct().getPrice() * i.getCount(); %>
		<tr>
			<td> <%= i.getProduct().getName() %> </td>
			<td> <%= i.getProduct().getPrice() %> </td>
			<td> <%= i.getCount() %> </td>
			<td> <%= price %> </td>
			<td>
				<form method="post" action="ShoppingCartServlet">
		 			<input type="hidden" name="deleteItemId" value="<%= i.getId() %>" />
		 			<input type="submit" value="Delete" />
				</form>
			</td>
		</tr>
	<% total += price;
     } %>

    </table>

    <p>
    In total: <%= total %> dinars.
    </p>

    <p>
    <a href="webShop.jsp">Back</a>
    </p>

</body>
</html>