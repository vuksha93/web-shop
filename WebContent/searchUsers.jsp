<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ page import="servlet1.dao.UserDAO" %>
<%@ page import="servlet1.webshop.User" %>
<%@ page import="java.util.List" %>

<%@ include file="checkUser.jsp" %>
<%-- <jsp:include page="checkUser.jsp" /> --%> 
    
<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Search users</title>

</head>
<body>

	<form action="searchUsers.jsp" method="get">
        Enter user: <input type="text" name="user" autofocus /><br/>            
        <input type="submit" value="Search"> 
    </form> 

	<% 
	String searchParam = request.getParameter("user");
	if(searchParam != null) {
	    String strUser = searchParam.toLowerCase(); 		
		List<User> users = UserDAO.search(strUser);
	
		for (User usr : users) { %>
		    <p> <%= usr.getUsername() %> </p>
		<% } %>   
		 
	<% } %>
		
	
	<p>
		<a href="webShop.jsp">Back</a>
    </p>

</body>
</html>